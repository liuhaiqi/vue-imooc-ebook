import Storage from 'web-storage-cache'

const localStorage = new Storage()

export function setLocalStorage (key, value) {
  return localStorage.set(key, value)
}

export function getLocalStorage (key) {
  return localStorage.get(key)
}

export function removeLocalStorage (key) {
  return localStorage.delete(key)
}

export function clearLocalStorage () {
  return localStorage.clear()
}

export function setBookObject (fileName, key, value) {
  let book = getLocalStorage(`${fileName}-info`)
  if (!book) {
    book = {}
  }
  book[key] = value
  setLocalStorage(`${fileName}-info`, book)
}

export function getBookObject (fileName, key) {
  let book = getLocalStorage(`${fileName}-info`)
  if (book) {
    return book[key]
  } else {
    return null
  }
}

// 字体到localStorage
export function getFontFamily (fileName) {
  return getBookObject(fileName, 'fontFamily')
}

export function saveFontFamily (fileName, font) {
  return setBookObject(fileName, 'fontFamily', font)
}

// 字号的到localStorage
export function getFontSize (fileName) {
  return getBookObject(fileName, 'fontSize')
}

export function saveFontSize (fileName, fontSize) {
  return setBookObject(fileName, 'fontSize', fontSize)
}

// 国际化
export function getLocale () {
  return getLocalStorage(`locale`)
}

export function saveLocale (locale) {
  return setLocalStorage(`locale`, locale)
}

// 主题
export function getTheme (fileName) {
  return getBookObject(fileName, 'theme')
}

export function saveTheme (fileName, theme) {
  return setBookObject(fileName, `theme`, theme)
}

// 保存章节信息
export function saveLocation (fileName, location) {
  return setBookObject(fileName, `location`, location)
}

export function getLocation (fileName) {
  return getBookObject(fileName, 'location')
}

export function getReadTime (fileName) {
  return getBookObject(fileName, `readtime`)
}

export function saveReadTime (fileName, readTime) {
  return setBookObject(fileName, `readtime`, readTime)
}
