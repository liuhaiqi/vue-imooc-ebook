import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引用字体图标
import './assets/styles/icon.css'
// 引用web字体
import './assets/fonts/daysOne.css'
// 引入全局样式文件
import './assets/styles/global.scss'
// 引入i18n国际化
import i18n from './lang'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
